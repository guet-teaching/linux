1.命名约定
 1) 长度最大可达255个字节
 2）可以包含特殊字符，除了/
 3）区分大小写 case-sensitive
 4) 可以包含大写和小写的字母
 5）不应该有空格
2.主目录 ，home， ~
3.文件类型
  1）普通文件
  2）目录文件
  3）特殊文件
4.用户类型
  1）系统管理员 Administrator, root 根用户
  2）文件所有者 File Owner
  3）组所有者 Group owner
  4）其他用户 Other users
5. pwd (print working directory)
6. cd （change directory）
   cd .. 当前目录的父目录
   cd 不带路径的时候，表示返回主目录
   
7.mkdir (make directory) 创建目录
8.rmdir(remove directory) 删除目录
9.ls 显示目录中文件和子目录的名称
  ls -l 显示文件和目录的详细列表
  ls -al 和ls -la 和 ls -a -l 和 ls -l -a 作用是相同的

10.cat (concatenate):显示指定文件的内容
11.cp(copy):将源文件内容复制到目标文件
12.rm(remove):删除文件或目录
13.mv(move):移动文件或者重命名

linux技巧：使用键盘的Tab键，自动补全

创建文件：info ls --output file1
  