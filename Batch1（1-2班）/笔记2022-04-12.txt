回顾：
高级shell脚本：
1.命令的替换：使用反单引号 `` 
2.expr ：计算算术表达式，注意 操作符两侧需要有空格
  命令 expr 4 + 5
  expr 不支持小数
  使用乘法时，需要加上 \* 
  保存表达式的值到变量： num=`expr 2 + 3`
3.test :计算表达式并且返回True(0)或False(1),
  可以使用[] 替换，注意：[] 表达式两边必须有空格
  test $name="Jack"  =》 [       $name="Jack"       ]
  -a :and ,与
  -o : or ，或
  !  : not ，非
  
activity6.1:
 unanswered=`expr $total - $answered`
 pending=$((totalqueries-answered))
 
程序结构：
1.if 结构：
	echo "enter a number:"
	read number
	if test $number -gt 60
	then
	echo "合格."
	else
	echo "不合格."
	fi

activit6.2:
	echo "enter a grade:"
	read grade
	if test $grade -le 80
	then
	echo "Average."
	elif [    $grade -gt 80 -a $grade -le 90    ]
	then
	echo "Good."
	else
	echo "Outstanding."
	fi
2.case 结构：
	echo "            Welcome             "
	echo "================================"
	echo "||1,Global Roam    2,V-mail   ||"
	echo "||3,Mail-on-move   4,Caller-ID||"
	echo "||5,Dial-a-pizza              ||"
	echo "================================"
	echo "Pls Enter your service:"
	read choice
	case $choice in
	1)
	echo "global roam.......";;
	2)
	echo "v-mail.......";;
	3)
	echo "mail-on-move......";;
	4)
	echo "caller-id........";;
	5)
	echo "dial-a-pizza.......";;
	*)
	echo "no this service，出门左拐！"
	esac
3.while 结构：当条件表达式为真，进入循环
	i=1
	while test $i -le 10
	do
	  echo $i
	 # i++
	 # i=`expr $i + 1`
	 i=$((i+1))
	done
4.until 循环：当条件表达式为真，退出循环
	i=1
	until test $i -gt 10
	do
	echo $i
	i=$((i+1))
	done

5.for 结构：
for fruit in apple banana peach
do
echo $fruit
done


6.exit 或 exit 0 :表示退出shell脚本

7.test 还可以用来检查文件状态
  test -e filename :file exists 文件存在
  test -f filename: 文件存在并且是普通文件
  test -d filename: 文件存在并且是目录
8. 字符串判断
   string 字符串不为空
   -z 字符串长度为0
   -n 字符串长度不为0
   =
   !=

  var_a="Jack"
  var_b="jack"
  test $var_a!=$var_b 
  test -n $var_a

9.break：结束循环
  continue:跳过当前循环，继续下一次循环
   

算术比较：
1.-eq :equal to,=
2.-ne ： not equal to , !=
3.-gt ： greater than , >
4.-ge : greater than or equal to, >=
5.-lt :less than ,<
6.-le :less than or equal to , <=

10.函数，方法
使用关键字function创建（定义）函数，
通过函数名来调用函数，但是必须先定义再调用

#sayHello
# function 函数定义
function sayHello
{
 echo "Hello Linux."
}
echo "hello."
# 调用函数
sayHello

参数：
add 1  6
$0  $1 $2

$0:函数名
$1:第1个参数
$2:第2个参数
$*:所有参数
$#:所有参数的个数

add 1 6，其中$*=1 6,$#=2

test a  b c d e f g h i j  k
    $1  $2            9  10 11
shift 2
test c d e f g h i j  k
     $1  $2          $8 	
shift 左移参数

调试脚本：
-v:在执行shell脚本前，先打印shell脚本在屏幕
命令： bash -v test.sh
-x:把实际执行的每一个命令显示，通过+表示
命令： bash -x test.sh












 
 
  

   
