﻿回顾：
1.file-naming conventions 命名约定
1）长度最大可达256字节
2）可以包含特殊字符，但是/ 符号除外
3）可以包含大写小写字母
4）区分大小写 case-sensitive
5）不应该有空格或者tab

home ，主目录，~

文件类型type of files：
Ordinary file 普通文件
Directory file 目录文件
Special file 特殊文件

命令：
1）pwd 显示当前目录的完整路径
2）cd 切换目录
   cd ..:当前目录的父目录（返回上一级）
   cd:不带路径，返回主目录
3）mkdir:创建目录
4）rmdir:移除目录
        需要同时满足2个条件：1.Empty ,目录为空;2.Not the current directory 不是当前目录
5）ls:显示目录中的文件和子目录的名称
    ls -l:显示文件和目录的详细列表
	.(a dot)代表当前目录，..（two dot）代表父目录

6）cat(concatenate连接)：显示指定文件的内容
      可以垂直连接多个文件的内容
7）cp(copy)：将源文件内容复制到目标文件
8）rm(remove):删除文件或目录
9）mv(move)：移动文件或者更改文件名
10）more ,less ：用于一次显示一屏内容

11）重点：wildcard characters 通配符
*:匹配任意多个字符，包含0个，1个，多个
？：匹配1个字符
[]:与指定的一组字符中的1个匹配

abc*:abc,abcd,abcdefg,abc123,123abc,ab1c
	可以匹配的有：abc,abcd,abcdefg,abc123
	不可以匹配的有：123abc,ab1c
*ab：abc,1ab,12ab
	可以匹配的有：1ab,12ab
	不可以匹配的有：abc
abc*:表示以abc开头的任意内容；
*ab:表示以ab结尾的任务内容；

abc？:abc,abcd,abcdefg,ab1c
	可以匹配的有：abcd
	不可以匹配的有：abc,abcdefg,ab1c
abc[12]:abc,abcd,abc1,abc2,abc3,abc123,abc12
    可以匹配的有：abc1,abc2
    不可以匹配的有:abc,abc123,abc12,abcd,abc3
ab[cd]e:只能匹配abce,abde
        123abce,abce123



linux操作系统，所有的操作都需要权限！！
     
小提示：Linux操作系统的tab键:自动补全

授权/赋予权限

