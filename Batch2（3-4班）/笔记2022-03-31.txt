﻿回顾：
过滤器：
1）grep 
     1. [] 匹配一组字符中的任意1个
	 2. [] with Hypheh 匹配一排字符中的1个
	 3. ^ 匹配每行的开头
	 4. ^ within [] 匹配除了后面一排字符的1个
	 5. $ 匹配每行的结尾
	 6. . 匹配任意1个
	 7. \ 忽略后面字符的特殊含义
2）wc 计算文件中的行数，单词数，字符数
3）cut 从命令的输出结构抽取特定列
     -f 显示指定列
	 -c 显示指定字符
	 -d 指定列分隔符
4)tr用于将一组字符转换为另外一组字符
5）sort 按照升序排序
 
 
使用vi编辑器创建文件 students ，用来保存学生的学号、姓名和电话信息，文件内容为：
  20200301:zhangsan:18177981235
  20200302:lisi:15578486666
  20210303:wangming:18381070102
  20210304:lihong:18220208899
  20220305:zhangliang:18498270102
使用过滤器，完成以下功能： 
1.找出姓zhang的学生；  
 命令 grep 'zhang' students 
2.找出学号为2020开头的学生；
命令 grep '^2020' students 
3.找出手机号为181，182或183开头的学生； 
命令 grep '18[1-3]' students 
4.找出手机尾号为0102的学生；
命令 grep '0102$' students 
5.获取该文件中所有学生的姓名，并保存到文件names中；   
命令 cut -d':' -f2 students > names


sort 按照升序排序
     -r 降序
	 -f 按字符的ASCII值顺序排列
	 -n 按非ASCII序列中的数字排序
	 filename 对文件内容排序
	 +pos1 -pos2 按特定列的顺序对文件排序
	 -t 创建具有不同列分隔符的文件
	 -o 保存输出到文件
	 
管道，过滤器或者命令的标准输出作为标准输入发送到另外一个过滤器或者命令
管道符号 | 
命令： ls -l | cut -d' ' -f1
       cat students | grep 'zhang'
	   cat test1 | sort
	   cat students | grep 'zhang' | cut -d':' -f2
	   

tee命令：接收标准输入并写入标准输出文件
    命令： cat test1 | tee test2
	
	
第5章 : basic shell scripting
shell 脚本，shell编程

shell的特点
job control 作业控制
 前台作业和后台作业
 前台作业需要用户交互
bg : 将作业发送到后台
fg ：将已挂起的作业带回前台
 
 
shell 解释命令的过程：
 1.shell 在屏幕打印提示信息
 2.用户输入命令
 3.shell解释并执行命令
 4.shell 等待命令执行完成

    
 