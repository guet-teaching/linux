﻿回顾：
结束终止进程：
-15：正常结束，finished,默认信号
-9：强制终止进程，killed

命令：
kill  pid，kill -15 pid，kill -9 pid
killall name,killall -15 name,killall -9 name

第8章：
压缩文件： gzip a.txt
解压文件：gunzip a.txt.gz 或 gzip -d a.txt.gz


-----重点--------------
tar：打包命令
-c:create ,创建文件
-x:Extract, 抽取，提取文件
-f:filename ,指定文件名
-t：diaplay or list，展示文件列表
-v: verbose ,展示详细的信息
-z: 使用gzip来压缩/解压文件
-r: 往已存在的压缩文件添加文件

命令1：tar -cf mydoc.tar a.txt b.txt 
-cf ，创建文件

命令2：tar -tvf mydoc.tar 
-tvf，查看详细的文件列表

命令3：tar -xvf mydoc.tar
-xvf，从压缩包提取文件（解压）

命令4：tar -rvf mydoc.tar c.txt
-rvf:添加新文件c.txt到压缩包mydoc.tar中

命令5：tar -czf mydoc.tar.gz a.txt b.txt c.txt 
-czf：压缩并打包文件
-----重点--------------

其他工具：
1.bc:计算器 
2.finger:查看系统用户的状态
3.ispell:拼写检查
4.chfn:修改用户的finger信息
5.head:从文件头部指定行查看
  tail：从文件底部指定行查看

UID:user id
root账号：id=0

添加账号：
命令 : useradd 

账号的主目录,默认在/home/xxxx

-----重点-----
命令：useradd -d /data/niit002 niit002
-d:表示修改账号的主目录（change home directory）
-----重点-----

chmod:
符号：
u g o + - r w x
绝对：
4 2 1
776

Grant 授予 Revoke 撤销 root 权限：
1.useradd
2.usermod ，-s 修改账号的shell
3./etc/passwd 配置文件

-----重点-----
删除用户：
命令：userdel
userdel -r: 删除用户的主目录 
-----重点-----


 














