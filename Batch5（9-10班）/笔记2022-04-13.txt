﻿回顾：
1.if test ,[  ]
  then
  elif []
  then
  
  else
  
  fi
2.case $var in
  value1)
  command;;
  value2)
  command;;
  *)
  command;;
  esac
3.while [ ]
  do
  
  done
4.until [ ] 
  do
  
  done
5.for var in value1 value2 value3
  do
  
  done

6.字符串判断

  test -z "$name"  
  [ -n "$name"]
  $name="steve"
  $name!="steve"

7.break:退出循环，终止循环
   continue:跳过当前循环，继续执行下一次的循环
   
8.函数 function
	# hello
	# 创建（定义）函数
	function hello
	{
	echo "Hello Linux."
	}
	# 调用函数，通过函数名调用,先定义函数再调用
	hello
	
参数：
add 2  4  =》 $*=2 4   $#=2	
$0  $1 $2
$0:表示函数名称
$1:表示第1个参数
$2:表示第2个参数
...
$9:表示第9个参数
$*:表示所有参数
$#:表示所有参数的个数


shift 左移参数

调试shell脚本：
-v:在执行脚本的时候，先将shell脚本显示在显示器
命令：bash -v test.sh
-x:把实际执行的每一个命令显示出来，通过+表示 
命令：bash -x test.sh


pwd :print working directory
cd
cd .
cd ..
cd ~
cd /
mkdir
rmdir
rm
ls

touch filename :创建一个空文件

第7章：文件和进程

1.find 在特定目录以及子目录下查找文件
  -name 按文件名查找
  find -name "add.sh"
  -type 按文件类型查找,f表示普通文件， d表示目录
  find -type f
  -mtime 按当前日期特定天数内文件的修改时间查找
  find -mtime n ：查找正好n天之前修改的文件，exactly
  find -mtime +n ：查找大于n天之前修改的文件,earlier than
  find -mtime -n:查找小于n天之前修改的文件, less than

问题：找出所有以".sh"结尾的文件，请写出命令。
	

  