﻿回顾：
1.find -name 按照文件名查找
  find -type 文件类型
  find -mtime n 刚好是n天之前修改的文件
  find -mtime +n 大于n天之前修改的文件
  find -mtime -n 小于n天之前修改的文件

问题：找出所有以".sh"结尾的文件，请写出命令。
find -name "*.sh"
  
2.locate 命令 ：最快最简单的查找方法，非实时查找
         需要以root账号执行命令updatedb，更新文件
		 locate filename
		 
多文件处理
1.paste:并排显示文件的内容，水平合并
2.cmp :比较两个文件差异，显示两个文件第一个不同的行号和字符号
3.comm:逐行比较两个已排序的文件
       排序命令：sort data2 > data3
	   比较命令：comm data1 data3
       比较结果有3列
	   第1列只出现在第1个文件
	   第2列只出现在第2个文件
	   第3列同时出现在第1，2个文件
3.uniq：显示文件中重复或唯一的行
  命令：sort data1|uniq
  -u：只显示唯一的行,uniq  ，命令：sort data1|uniq -u
  -d:显示重复的行，duplicate ，命令：sort data1|uniq -d
  -c:显示每一行出现的次数,count ，命令：sort data1|uniq -c

特殊字符：
 \ ：忽略后面的字符的特殊含义,当前普通字符
 ''：忽略单引号内的所有特殊字符的含义
 ""：忽略双引号内的所有特殊字符的含义，除了$,',\
例子： 
a=linux
echo "this value of variable \"a\" is equal to $a"

this value of variable \"a\" is equal to $a
this value of variable \"a\" is equal to linux
this value of variable "a" is equal to linux（正确）
this value of variable \a\ is equal to linux


进程Process：
PID:process ID,进程ID
PPID:parent process ID ,父进程ID

Linux启动过程
init->getty-login->shell




	

  